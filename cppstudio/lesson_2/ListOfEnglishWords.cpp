#include "ListOfEnglishWords.hpp"
//#include <QDebug>

const std::string THIS_WORD = "Данное слово: ";
const std::string SUCC_REMOVE = " успешно удалено";
const std::string WORD_NOT_EXIST_FOR_TRANSLATE =
    "Данного слова нет, чтобы добавить в словарь и добавить к нему транскрипцию и переводы, нажмите кнопку 'Добавить слово')";

ListOfEnglishWords::ListOfEnglishWords(ListOfEnglishWords &&list) :
  head_(list.head_),
  tail_(list.tail_),
  size_(list.size_)
{
  list.head_ = nullptr;
  list.tail_ = nullptr;
  list.size_ = 0;
}

ListOfEnglishWords::~ListOfEnglishWords()
{
  while (head_)
  {
    ListOfEnglishWords::Node *temp = ListOfEnglishWords::head_;
    head_ = head_->next_;
    delete temp;
  }
}

int ListOfEnglishWords::insert(std::string &transcription, std::string &word, std::string &lineOfTranslations)
{
  int result = -2;
  if (head_ == nullptr)
  {
    head_ = new Node(transcription, word, lineOfTranslations);
    tail_ = head_;
    ++size_;
    result = 0;
  } else
  {
    ListOfEnglishWords::Node *temp = head_;
    while (temp)
    {
      if (temp->word_ == word)
      {
        result = 1;
        break;
      }
      temp = temp->next_;
    }
    if (temp == nullptr)
    {
      ListOfEnglishWords::Node *temp = new Node(transcription, word, lineOfTranslations, nullptr);
      tail_->next_ = temp;
      tail_ = temp;
      ++size_;
      result = 0;
    }
  }
  return result;
}

std::string ListOfEnglishWords::removeEnglishWord(std::string &word)
{
  std::string result = "No";
  if (head_ != nullptr)
  {
    ListOfEnglishWords::Node *temp = head_;
    if (temp->word_ == word)
    {
      this->deleteHead();
      result = THIS_WORD + word + SUCC_REMOVE;  //измемнить
      //      QString string = QString::fromStdString(word);
      //      qDebug() << "List:"<<string;
    } else
    {
      ListOfEnglishWords::Node *temp = head_->next_;
      ListOfEnglishWords::Node *curr = head_;
      while (temp != nullptr)
      {
        if (temp->word_ == word)
        {
          curr->next_ = temp->next_;
          delete temp;
          result = THIS_WORD + word + SUCC_REMOVE;
          //          QString string = QString::fromStdString(word);
          //          qDebug() << "List:" << string;
          break;
        }
        temp = temp->next_;
      }
    }
  }
  return result;
}

void ListOfEnglishWords::deleteHead()
{
  Node *temp = head_;
  head_ = head_->next_;
  delete temp;
  size_--;
}

void ListOfEnglishWords::deleteNode(ListOfEnglishWords::Node *node)
{
  Node *temp = head_;
  while (temp->next_ != node)
  {
    temp = temp->next_;
  }
  temp->next_ = node->next_;
  delete node;
  size_--;
}

int ListOfEnglishWords::getSize() const
{
  return size_;
}

void ListOfEnglishWords::insertListOfTranslations(ListOfEnglishWords::Node *node)
{
  {
    if (head_ == nullptr)
    {
      head_ = new Node(node);
      tail_ = head_;
      ++size_;
    }
    else
    {
      ListOfEnglishWords::Node *temp = new Node(node, nullptr);
      tail_->next_ = temp;
      tail_ = temp;
      ++size_;
    }   //++size  вынести/удалить
  }
}
ListOfEnglishWords &ListOfEnglishWords::operator=(ListOfEnglishWords &&list) noexcept
{
  head_ = list.head_;
  tail_ = list.tail_;
  size_ = list.size_;
  list.head_ = nullptr;
  list.tail_ = nullptr;
  list.size_ = 0;
  return *this;
}

std::ostream &operator<<(std::ostream &out, ListOfEnglishWords &list)
{
  ListOfEnglishWords::Node *temp = list.head_;
  while (temp != nullptr)
  {
    out << temp->word_ << " " << temp->transcription_ << *temp->listOfTranslations_;
    out << "\n";
    temp = temp->next_;
  }
  return out;
}
std::string ListOfEnglishWords::translate(std::string &word) const
{
  std::string translation = "Транскрипция '";
  translation += word;
  translation += "': ";
  Node *temp = head_;
  while (temp)
  {
    if (temp->word_ == word)
    {
      translation += temp->transcription_;
      //      QString result = QString::fromStdString(temp->transcription_);
      //     qDebug() << "transsss:" << result;
      translation += temp->listOfTranslations_->translate();
      break;
    }
    temp = temp->next_;
  }
  if (temp == nullptr)
  {
    translation = WORD_NOT_EXIST_FOR_TRANSLATE;
  }
  return translation;
}
int ListOfEnglishWords::addTranslation(std::string &word, std::string &lineOfTranslations)
{
  int result = -2;
  if(head_ != nullptr)
  {
    ListOfEnglishWords::Node *temp = head_;
    while (temp != nullptr)
    {
      if (temp->word_ == word)
      {
        temp->listOfTranslations_->insert(lineOfTranslations);
        result = 0;
        break;
      }
      temp = temp->next_;
    }
    if(temp == nullptr)
    {
      result = -1;
    }
  }
  else
  {
    result = -1;   // не найдено слово
  }
  return result;
}

int ListOfEnglishWords::editTranscription(std::string &word, std::string &transcriptoin)
{
  int result = -2;
  if(head_ != nullptr)
  {
    ListOfEnglishWords::Node *temp = head_;
    while (temp != nullptr)
    {
      if (temp->word_ == word)
      {
        temp->transcription_ = transcriptoin;
        result = 0;
        break;
      }
      temp = temp->next_;
    }
    if(temp == nullptr)
    {
      result = -1;
    }
  }
  else
  {
    result = -1;   // не найдено слово
  }
  return result;
}



