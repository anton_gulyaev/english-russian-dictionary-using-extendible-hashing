#ifndef SECOND_GIT__DYNAMICARRAY_HPP_
#define SECOND_GIT__DYNAMICARRAY_HPP_

#include <stdexcept>
#include <memory>

namespace gulyaev
{
  namespace detail
  {
    template <class T>
    class DynamicArray
    {
    public:
      DynamicArray();
      explicit DynamicArray(size_t size);
      DynamicArray(const DynamicArray <T> &dynamicArray);
      DynamicArray(DynamicArray <T> &&dynamicArray) noexcept;
      ~DynamicArray() = default;

      T &operator[](size_t index) const;
      DynamicArray <T> &operator=(const DynamicArray <T> &dynamicArray);
      DynamicArray <T> &operator=(DynamicArray <T> &&dynamicArray) noexcept;

      size_t getAmount() const;
      size_t getSize() const;
      void add(const T &element);
      void remove(size_t index);
      void clear();

    private:
      std::unique_ptr <T[]> dynamicArray_;
      size_t size_;
      size_t amountOfElements_;
    };
  }
}

template <class T>
gulyaev::detail::DynamicArray <T>::DynamicArray() :
  dynamicArray_(nullptr),
  size_(0),
  amountOfElements_(0)
{}

template <class T>
gulyaev::detail::DynamicArray <T>::DynamicArray(const size_t size) :
  dynamicArray_(new T[size]),
  size_(size),
  amountOfElements_(0)
{}

template <class T>
gulyaev::detail::DynamicArray <T>::DynamicArray(const DynamicArray <T> &dynamicArray) :
  dynamicArray_(new T[dynamicArray.size_]),
  size_(dynamicArray.getSize()),
  amountOfElements_(dynamicArray.getAmount())
{
  for (size_t i = 0; i < size_; ++i)
  {
    dynamicArray_[i] = dynamicArray[i];
  }
}

template <class T>
gulyaev::detail::DynamicArray <T>::DynamicArray(DynamicArray <T> &&dynamicArray) noexcept :
  size_(dynamicArray.size_),
  amountOfElements_(dynamicArray.getAmount())
{
  dynamicArray_.swap(dynamicArray.dynamicArray_);
}

template <class T>
T &gulyaev::detail::DynamicArray <T>::operator[](const size_t index) const
{
  if (index < size_ || size_ <= 0)
  {
    return dynamicArray_[index];
  }
  throw std::out_of_range("Out of range");
}

template <class T>
gulyaev::detail::DynamicArray <T> &gulyaev::detail::DynamicArray <T>::operator=(const DynamicArray <T> &dynamicArray)
{
  if (this != dynamicArray)
  {
    dynamicArray_.reset(new T[size_]);
    size_ = dynamicArray.getSize();
    
    for (size_t i = 0; i < size_; ++i)
    {
      dynamicArray_[i] = dynamicArray[i];
    }
  }
  return *this;
}

template <class T>
gulyaev::detail::DynamicArray <T> &gulyaev::detail::DynamicArray <T>::operator=(DynamicArray <T> &&dynamicArray) noexcept
{
  if (this != &dynamicArray) {
    amountOfElements_ = dynamicArray.amountOfElements_;
    size_ = dynamicArray.size_;
    dynamicArray_.swap(dynamicArray.dynamicArray_);
  }

  return *this;
}

template <class T>
void gulyaev::detail::DynamicArray <T>::add(const T &element)
{
  if (amountOfElements_ == size_)
  {
    std::unique_ptr <T[]> temp = std::make_unique <T[]>((size_ + 1) * 2);
    for (size_t i = 0; i < size_; ++i)
    {
      temp[i] = dynamicArray_[i];
    }
    temp[size_] = element;
    amountOfElements_++;
    size_ = (size_ + 1) * 2;
    dynamicArray_.swap(temp);
  } else
  {
    this->dynamicArray_[size_ - (size_ - amountOfElements_)] = element;
    amountOfElements_++;
  }
}

template <class T>
void gulyaev::detail::DynamicArray <T>::remove(const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Out of range");
  }
  for (size_t i = index; i < size_ - 1; ++i)
  {
    dynamicArray_[i] = dynamicArray_[i + 1];
  }
  --amountOfElements_;
}

template <class T>
size_t gulyaev::detail::DynamicArray <T>::getSize() const
{
  return size_;
}

template <class T>
void gulyaev::detail::DynamicArray <T>::clear()
{
  dynamicArray_.reset();
  size_ = 0;
}
template <class T>
size_t gulyaev::detail::DynamicArray <T>::getAmount() const
{
  return amountOfElements_;
}

#endif //HASH_TABLE__DYNAMICARRAY_HPP_
