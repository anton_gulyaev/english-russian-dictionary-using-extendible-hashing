#include "HashTable.hpp"
//положить в проект
#include <QString>
#include <QDebug>

const std::string WORD_NOT_EXIST = "Данного слова нет в словаре.";
const std::string WRONG_WORD = "-1";

HashTable::HashTable(int size) :
  amountOfElemets_(0),
  capacityOhHashTable_(size)
{
  array_ = new ListOfEnglishWords *[capacityOhHashTable_];
  for (int i = 0; i < capacityOhHashTable_; i++)
  {
    array_[i] = new ListOfEnglishWords();
  }
}

HashTable::HashTable(HashTable &&hashTable) noexcept:
  array_(hashTable.array_),
  amountOfElemets_(hashTable.amountOfElemets_),
  capacityOhHashTable_(hashTable.capacityOhHashTable_)
{
  hashTable.array_ = nullptr;
  hashTable.amountOfElemets_ = 0;
  hashTable.capacityOhHashTable_ = 0;
}


HashTable::~HashTable()
{
  amountOfElemets_ = 0;
  for (int i = 0; i < capacityOhHashTable_; i++)
  {
    delete array_[i];
  }
  capacityOhHashTable_ = 0;
  delete[] array_;
}

int HashTable::insertWord(std::string englishWord, std::string transcription, std::string lineOfTranslations)
{
  long long qIndex = hashFunction(englishWord);
  int result = -2;
  if(qIndex != -1)
  {
    int resultOfAdding = array_[qIndex]->insert(transcription, englishWord, lineOfTranslations);
    if(resultOfAdding == 0)
    {
      ++amountOfElemets_;
      result = 0;
    }
    else
    {
      result = 1;  //слово уже есть
    }
  }
  else
  {
    result = -1;    //неверное слово
  }
  return result;
}

std::string HashTable::remove(std::string &word)
{
  std::string result;
  long long index = search(word);
  if (index < 0 || index > capacityOhHashTable_)    //изменить имя capacityOhHashTable_ на понятное и длинное
  {

    result = WORD_NOT_EXIST;   //слова нет в словаре
  }
  else
  {
    result = array_[index]->removeEnglishWord(word);  //bool
    if(result == "No")
    {
      result = WORD_NOT_EXIST;
    }
    else
    {
      --amountOfElemets_;
      QString string = QString::fromStdString(result);
      qDebug() << "Hash:"<<string;
    }
  }
  return result;
}

long long HashTable::hashFunction(std::string &s) const
{
  long long hashValue = 0;
  if(!s.empty())
  {
    const int DIGIT_FOR_HASH = 31;
    long long powOdDigit = 1;
    for (char c : s)
    {
      hashValue = (hashValue + (c - 'a' + 1) * powOdDigit) % capacityOhHashTable_;
      powOdDigit = (powOdDigit * DIGIT_FOR_HASH) % capacityOhHashTable_;
    }
  }
  else
  {
    hashValue = -1;
  }
  return hashValue;
}

long long HashTable::search(std::string &word) const
{
  long long resultIndex = 0;
  resultIndex = hashFunction(word);
  return resultIndex;
}

std::string HashTable::translate(std::string &word)
{
  return array_[search(word)]->translate(word);
}


int HashTable::insert(std::string englishWord, std::string transcription, std::string lineOfTranslations)
{
  int result = -2; //что-то не так
  if(!englishWord.empty())
  {
    const int OCCUPANCY = 3 * capacityOhHashTable_ / 4;
    if (amountOfElemets_ < OCCUPANCY)
    {
      result = insertWord(englishWord, transcription, lineOfTranslations);
    }
    else
    {
      HashTable *table = new HashTable(capacityOhHashTable_ * 2);
      for (int i = 0; i < capacityOhHashTable_; i++)
      {
        ListOfEnglishWords::Node *temp = array_[i]->head_;
        while (temp != nullptr)
        {
          qDebug() << "resize: "<< table->capacityOhHashTable_;
          table->insertListOfTranslations(temp);
          temp = temp->next_;
        }
      }
      *this = std::move(*table);
      result = insertWord(englishWord, transcription, lineOfTranslations);
    }
  }
  return result;
}

int HashTable::getM() const
{
  return capacityOhHashTable_;
}

int HashTable::getCurrArraySize() const
{
  return amountOfElemets_;
}

void HashTable::insertListOfTranslations(ListOfEnglishWords::Node *node)
{
  ListOfEnglishWords::Node *temp = node;
  long long index = hashFunction(temp->word_);
  array_[index]->insertListOfTranslations(temp);
  ++amountOfElemets_;
}

HashTable &HashTable::operator=(HashTable &&hashTable) noexcept
{
  array_ = hashTable.array_;
  amountOfElemets_ = hashTable.amountOfElemets_;
  capacityOhHashTable_ = hashTable.capacityOhHashTable_;
  hashTable.array_ = nullptr;
  hashTable.amountOfElemets_ = 0;
  hashTable.capacityOhHashTable_ = 0;
  return *this;
}

std::ostream &operator<<(std::ostream &out, HashTable &hashTable)
{
  for (int i = 0; i < hashTable.capacityOhHashTable_; ++i)
  {
    if (hashTable.array_[i]->head_)
    {
      out << *hashTable.array_[i];
    }
  }
  return out;
}

int HashTable::addTranslation(std::string word, std::string lineOfTranslations)
{
  int result = -2;
  if(word != WRONG_WORD)
  {
    result = 0;
  }
  else
  {
    result = 1;
  }
  long long index = search(word);
  if (index > 0 && index <= capacityOhHashTable_ && result == 0)
  {
    result = array_[index]->addTranslation(word, lineOfTranslations);
  }
  return result;
}

int HashTable::editTranscription(std::string word, std::string transcription)
{
  int result = -2;
  word = toLower(word);
  if(word != WRONG_WORD)
  {
    result = 0;
  }
  else
  {
    result = 1;
  }
  long long index = search(word);
  if (index > 0 && index <= capacityOhHashTable_ && result == 0)
  {
    result = array_[index]->editTranscription(word, transcription);
  }
  return result;
}
