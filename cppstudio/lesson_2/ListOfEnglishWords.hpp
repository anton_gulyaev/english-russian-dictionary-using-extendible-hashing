#ifndef HASH_TABLE__LISTOFENGLISHWORDS_HPP_
#define HASH_TABLE__LISTOFENGLISHWORDS_HPP_
#include <iostream>
#include <utility>
#include <string>
#include <QString>
#include <QDebug>
#include "DynamicArray.hpp"

class ListOfTranslations
{
private:
  class Node
  {
  public:
    explicit Node(std::string word = " ", Node *next = nullptr) :
      word_(std::move(word)),
      next_(next)
    {};
    virtual ~Node() = default;
  public:
    std::string word_;
    Node *next_;

  };
  
  Node *head_;
  Node *tail_;
  int size_;

public:
  ListOfTranslations() :
    head_(nullptr),
    tail_(nullptr),
    size_(0)
  {};
  ListOfTranslations(ListOfTranslations &&list) noexcept;
  
  ~ListOfTranslations();
  
  ListOfTranslations &operator=(ListOfTranslations &&list) noexcept;
  int addTranslation(std::string &word, std::string &lineOftranslations);
  void insert(std::string &lineOftranslations);
  std::string translate() const;
  
  friend std::ostream &operator<<(std::ostream &out, ListOfTranslations &list);
  int getSize() const;
  
};

class ListOfEnglishWords
{

public:
  ListOfEnglishWords() :
    head_(nullptr),
    tail_(nullptr),
    size_(0)
  {};
  
  ListOfEnglishWords(ListOfEnglishWords &&list);
  ~ListOfEnglishWords();
  
  ListOfEnglishWords &operator=(ListOfEnglishWords &&list) noexcept;
  int insert(std::string &word, std::string &transcription, std::string &lineOftranslations);
  int addTranslation(std::string &word, std::string &lineOftranslations);
  int editTranscription(std::string &word, std::string &transcriptoin);
  std::string removeEnglishWord(std::string &word);
  int getSize() const;
  std::string translate(std::string &word) const;
  friend std::ostream &operator<<(std::ostream &out, ListOfEnglishWords &list);
  
  class Node
  {
  public:
    std::string word_;
    std::string transcription_;
    ListOfTranslations *listOfTranslations_; //& на список переводов
    Node *next_;


    explicit Node(std::string &transcription, std::string &englishWord, std::string &lineOfTranslations, Node *next = nullptr) :
      word_(std::move(englishWord)),
      transcription_(std::move(transcription)),
      next_(next)
    {
      //      QString result = QString::fromStdString(transcription_);
      //     qDebug() << "res:" << result;
      listOfTranslations_ = new ListOfTranslations();
      listOfTranslations_->insert(lineOfTranslations);
    }

    explicit Node(Node *node, Node *next = nullptr) :
      word_(node->word_),
      transcription_(node->transcription_),
      listOfTranslations_(std::move(node->listOfTranslations_)),
      next_(next)
    {}
    
    ~Node()
    {
      delete listOfTranslations_;
    }
  };
  void insertListOfTranslations(Node *node);
  void deleteHead();
  void deleteNode(ListOfEnglishWords::Node *node);
  
  Node *head_;
  Node *tail_;
  int size_;
  
};
#endif  //HASH_TABLE__LISTOFENGLISHWORDS_HPP_
