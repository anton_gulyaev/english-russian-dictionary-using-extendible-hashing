#include "ListOfEnglishWords.hpp"
#include <QDebug>
ListOfTranslations::ListOfTranslations(ListOfTranslations &&list) noexcept:
  head_(list.head_),
  tail_(list.tail_),
  size_(list.size_)
{
  list.head_ = nullptr;
  list.tail_ = nullptr;
  list.size_ = 0;
}

ListOfTranslations::~ListOfTranslations()
{
  while (head_)
  {
    Node *temp = dynamic_cast<Node *>(head_);
    head_ = head_->next_;
    delete temp;
  }
  head_ = nullptr;
  tail_ = nullptr;
}

std::string ListOfTranslations::translate() const
{
  std::string transcription;
  transcription += " Перевод: ";

  Node *temp = head_;
  while (temp)
  {
    transcription += temp->word_;
    transcription += " ";
    temp = temp->next_;
  }
  return transcription;
}

void ListOfTranslations::insert(std::string &lineOftranslations)
{
  gulyaev::detail::DynamicArray <std::string> array;
  std::string current;
  for (char i : lineOftranslations)
  {
    if (i != ' ')
    {
      current += i;
    } else
    {
      array.add(current);
      current = "";
    }
  }
  array.add(current);
  for (size_t i = 0; i < array.getAmount(); ++i)
  {
    if(head_ ==  nullptr)
    {
      head_ = new Node(array[i]);
      tail_ = head_;
      ++size_;
    }
    else
    {
      ListOfTranslations::Node *temp = new Node (array[i], nullptr);
      tail_->next_ = temp;
      tail_ = temp;
      ++size_;
    }
  }
}

int ListOfTranslations::getSize() const
{
  return size_;
}

ListOfTranslations &ListOfTranslations::operator=(ListOfTranslations &&list) noexcept
{
  head_ = list.head_;
  tail_ = list.tail_;
  size_ = list.size_;
  list.head_ = nullptr;
  list.tail_ = nullptr;
  list.size_ = 0;
  return *this;
}

std::ostream &operator<<(std::ostream &out, ListOfTranslations &list)
{
  ListOfTranslations::Node *temp = list.head_;
  while (temp)
  {
    out << " " << temp->word_;
    temp = temp->next_;
  }
  return out;
}








