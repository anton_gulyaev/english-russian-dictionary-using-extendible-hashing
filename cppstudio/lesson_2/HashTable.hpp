#ifndef HASH_TABLE__HASHTABLE_HPP_
#define HASH_TABLE__HASHTABLE_HPP_

#include <cassert>
#include <iostream>
#include "ListOfEnglishWords.hpp"
#include "Functions.hpp"
#include <string>
#include <QString>
#include <QtDebug>

class HashTable
{
private:

public:
  explicit HashTable(int size = 100);
  HashTable(HashTable &&hashTable) noexcept;
  ~HashTable();

  HashTable &operator=(HashTable &&hashTable) noexcept;
  int insert(std::string englishWord, std::string transcription, std::string lineOfTranslations);
  long long hashFunction(std::string &word) const;
  std::string translate(std::string &word);
  int addTranslation(std::string word, std::string lineOfTranslations);
  int editTranscription(std::string word, std::string transcription);
  friend std::ostream &operator<<(std::ostream &out, HashTable &hashTable);
  long long search(std::string &word) const;
  std::string remove(std::string &word);
  int getCurrArraySize() const;
  int getM() const;

private:
  int insertWord(std::string englishWord, std::string transcription, std::string lineOfTranslations);
  void insertListOfTranslations(ListOfEnglishWords::Node *node);

  ListOfEnglishWords **array_;
  int amountOfElemets_;
  int capacityOhHashTable_;

};

#endif //HASH_TABLE__HASHTABLE_HPP_
