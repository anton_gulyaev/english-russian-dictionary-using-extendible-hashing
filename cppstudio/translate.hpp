#ifndef TRANSLATE_HPP
#define TRANSLATE_HPP
#include <QtWidgets>
#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QtCore>
#include <QString>
#include <fstream>
#include "lesson_2//HashTable.hpp"
#include "Functions.hpp"


class Translate : QMainWindow
{
  Q_OBJECT;

public:
  Translate(QWidget *parent = 0);
  virtual ~Translate();

private:
  void show(QString &s);
  QString insert(QString &str);
  QString resultOfLoad_;

private slots:
  void translate();
  void clearingTranslation();;
  void addWord();
  void addTranslation();
  void removeWord();
  void saveAndQuit();
  void editTranscription();
  void downloadDict();

public:
  QLabel *labelTextEdit_;
  QLabel *labelLineEdit_;

  HashTable *hashTable_;

  QLineEdit *lineEdit_;
  QTextEdit *textEdit_;

  QPushButton *removeButton_;
  QPushButton *addButton_;
  QPushButton *clearTextEditButton_;
  QPushButton *translateButton_;
  QPushButton *saveAndQuitButton_;
  QPushButton *addTranslationButton_;
  QPushButton *editTranscriptionButton_;
  QPushButton *downloadDictButton_;
};

#endif // TRANSLATE_HPP
