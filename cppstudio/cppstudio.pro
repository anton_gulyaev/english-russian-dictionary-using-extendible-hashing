



TEMPLATE = app
SOURCES +=  lesson_2/main.cpp \
  Functions.cpp \
 lesson_2/HashTable.cpp \
  lesson_2/ListOfEnglishWords.cpp \
lesson_2/ListOfTranslations.cpp \
  translate.cpp
TARGET = cppstudio
QT += core gui
QT += widgets

greaterThan(QT_MAJOR_VERSION, 5.12.6): QT += widgets


CONFIG += debug
CONFIG += QT

HEADERS += \
  Functions.hpp \
  lesson_2/DynamicArray.hpp \
lesson_2/HashTable.hpp \
lesson_2/ListOfEnglishWords.hpp \
  translate.hpp

DISTFILES += \
  lesson_2/EnglishRussianDictionaryGulyaev.txt


