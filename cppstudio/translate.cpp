#include "translate.hpp"

const QString NAME_LABEL_TEXT_EDIT = "Поле перевода и сообщений";
const QString NAME_LABEL_LINE_EDIT = "Поле ввода";
const QString NAME_REMOVE_BUTTON = "Удалить слово";
const QString NAME_ADD_BUTTON = "Добавить слово";
const QString NAME_TRANSLATE_BUTTON = "Перевести";
const QString NAME_CLEAR_TEXT_EDIT_BUTTON = "Очистить";
const QString NAME_SAVE_BUTTON = "Сохранить изменения";
const QString NAME_ADD_TRANSLATION_BUTTON = "Добавить переводы";
const QString NAME_EDIT_TRANSCRIPTION_BUTTON = "Изменить транскрипцию";
const QString NAME_DOWNLOAD_DICT_BUTTON = "Загрузить словарь";

const QString ALL_SAVED = "Сохранено";
const QString FILE_NOT_CHOSEN = "Файл не выбран";
const QString SUCC_LOAD_DICT = "Словарь полностью готов к работе, все приготовления завершились без ошибок";
const QString NOT_FULL_LOAD = "В файле была(и) некорректная(ие) строка(и), словарь заполнился только корректными строками";

const QString WORD_ADD_SUCC = "Данное слово будет добавлено: ";
const QString WRONG_WORD = "Неверное слово, исправьте все ошибки и повторите попытку. ";
const QString WORD_ALREADY_IN_DICT = "Данное слово уже есть в словаре.";
const QString CHANGED_TRANSCRIPTION = "  изменена транскприция: ";
const QString TO_THIS_WORD = "К слову: ";
const QString WORD_NOT_EXIST = "Данного слова нет в словаре.";
const QString EMPTY_DICT = "Данные не были загружены, словарь пустой. Добавьте несколько слов, чтобы начать работу со словарём";
const QString ADD_TRANSLATIONS = "  добавлен(ы) перевеод(ы): ";
const QString VALIDATION = "Проверьте правильность введённых данных";

const QString LOAD_DICT = "Загрузка словаря";
const QString SAVE_CHANGES = "Соханение изменений";
const QString TXT = "*.txt";

const std::string WRONG_ENGLIH_WORD = "-1";

Translate::Translate(QWidget *parent) :
  QMainWindow(parent)
{
  //create HashTable
  hashTable_ = new HashTable(20);

  //create labels
  labelTextEdit_ = new QLabel(NAME_LABEL_TEXT_EDIT);
  labelLineEdit_ = new QLabel(NAME_LABEL_LINE_EDIT);

  //create buttons
  removeButton_ = new QPushButton(NAME_REMOVE_BUTTON);
  addButton_ = new QPushButton(NAME_ADD_BUTTON);
  translateButton_ = new QPushButton(NAME_TRANSLATE_BUTTON);
  clearTextEditButton_ = new QPushButton(NAME_CLEAR_TEXT_EDIT_BUTTON);
  saveAndQuitButton_ = new QPushButton(NAME_SAVE_BUTTON);
  addTranslationButton_ = new QPushButton(NAME_ADD_TRANSLATION_BUTTON);
  editTranscriptionButton_ = new QPushButton(NAME_EDIT_TRANSCRIPTION_BUTTON);
  downloadDictButton_ = new QPushButton(NAME_DOWNLOAD_DICT_BUTTON);

  //create textEdit
  textEdit_ = new QTextEdit();
  textEdit_->setReadOnly(true);

  //createLineEdit
  lineEdit_ = new QLineEdit();
  lineEdit_->setGeometry(QRect(QPoint(250, 50),
                               QSize(500, 50)));


  textEdit_->setGeometry(QRect(QPoint(250, 250),
                               QSize(50, 150)));

  //setPositionOnWindow
  translateButton_->setGeometry(QRect(QPoint(500, 500),
                                      QSize(50, 25)));

  connect(translateButton_, SIGNAL(clicked()), this,
          SLOT(translate()));

  connect(clearTextEditButton_, SIGNAL(clicked()), this,
          SLOT(clearingTranslation()));

  connect(addButton_, SIGNAL(clicked()), this,
          SLOT(addWord()));

  connect(removeButton_, SIGNAL(clicked()), this,
          SLOT(removeWord()));

  connect(saveAndQuitButton_, SIGNAL(clicked()), this,
          SLOT(saveAndQuit()));

  connect(addTranslationButton_, SIGNAL(clicked()), this,
          SLOT(addTranslation()));

  connect(editTranscriptionButton_, SIGNAL(clicked()), this,
          SLOT(editTranscription()));

  connect(downloadDictButton_, SIGNAL(clicked()), this,
          SLOT(downloadDict()));
}

Translate::~Translate()
{
  delete hashTable_;
}

void Translate::saveAndQuit()
{

  QString str = QFileDialog::getSaveFileName(0, SAVE_CHANGES, "", TXT);
  std::ofstream in;
  //file position
  std::string file = str.toUtf8().constData();
  char* cstr = new char[file.length() + 1];
  strcpy(cstr, file.c_str());
  in.open(cstr);
  std::ofstream out (cstr);
  if (out.is_open())
  {
    resultOfLoad_ = ALL_SAVED;
    qDebug() << "open";
    out << hashTable_->getCurrArraySize() << std::endl;
    out << *hashTable_;
  }
  else
  {
    resultOfLoad_ = FILE_NOT_CHOSEN;
  }
  out.close();
  delete []cstr;
  show(resultOfLoad_);
}

void Translate::removeWord()
{
  QString word = lineEdit_->text();
  std::string removeWord = word.toUtf8().constData();
  removeWord = toLower(removeWord);
  QString string;
  if(removeWord != WRONG_ENGLIH_WORD)
  {
    string = QString::fromStdString(hashTable_->remove(removeWord));
  }
  else
  {
    string = VALIDATION;
  }

  show(string);
}

void Translate::show(QString &result)
{
  textEdit_->setText(result);
  textEdit_->show();
}

QString Translate::insert(QString &str)
{
  QString *array = new QString[3];
  array = toPrettyFormWordWithTrans(str, array);
  std::string word = array[0].toUtf8().constData();
  std::string transcription = array[1].toUtf8().constData();
  std::string lineOfTranslation = array[2].toUtf8().constData();
  int add = -2;
  if(word != WRONG_ENGLIH_WORD)
  {
    add = hashTable_->insert(word, transcription, lineOfTranslation);
  }
  else
  {
    add = -1;
  }
  QString result;
  if(add == 0)
  {
    result = WORD_ADD_SUCC;
    result += QString::fromStdString(word) + " " + QString::fromStdString(transcription);
    result += " " + QString::fromStdString(lineOfTranslation);
//    show(result);
  }
  else if(add == -1)
  {
    result = WRONG_WORD;
//    show(result);
  }
  else if(add == 1)
  {

    result = WORD_ALREADY_IN_DICT;
//    show(result);
  }
  delete [] array;
  return result;
}

void Translate::editTranscription()
{
  QString str = lineEdit_->text();
  QString *array = new QString[3];
  array = toPrettyFormWordWithTrans(str, array);
  std::string word = array[0].toUtf8().constData();
  std::string transcriptionToAdd = array[1].toUtf8().constData();
  int add = -2;
  if(word != WRONG_ENGLIH_WORD)
  {
    add = hashTable_->editTranscription(word, transcriptionToAdd);
  }
  else
  {
    add = -1;
  }
  QString result;
  if(add == 0)
  {
    result = TO_THIS_WORD;
    result += QString::fromStdString(word);

    result += CHANGED_TRANSCRIPTION;
    result += QString::fromStdString(transcriptionToAdd);
    show(result);
  }
  else if(add == -1)
  {
    result = WRONG_WORD;
    show(result);
  }
  else if(add == 1)
  {
    result = WORD_NOT_EXIST;
    show(result);
  }
  delete [] array;
}

void Translate::downloadDict()
{
  QString str = QFileDialog::getOpenFileName(0, LOAD_DICT, "", TXT);
  std::ifstream in;
  //file position
  std::string file = str.toUtf8().constData();
  char* cstr = new char[file.length() + 1];
  strcpy(cstr, file.c_str());
  in.open(cstr, std::ios::in);
  if(!in.is_open())
  {
    resultOfLoad_ = EMPTY_DICT;
  }
  else
  {
    std::string line;
    getline(in, line);
    int num = 0;
    for (size_t i = 0; i < line.length(); i++)
    {
      num = num*10 + line[i] - 0x30;
    }

    num = (num  + 10) * 2;
    qDebug() << num;
    delete hashTable_;
    hashTable_ = new HashTable(num);
    qDebug() << hashTable_->getM();
    resultOfLoad_ = SUCC_LOAD_DICT;

    while (getline(in, line))
    {
      if(!line.empty())
      {
        QString str = QString::fromStdString(line);
        QString result = insert(str);
        if(result != WORD_ADD_SUCC)
        {
          if(result != WORD_ALREADY_IN_DICT)
          {
            if(result == WRONG_WORD)
            {
              resultOfLoad_ = NOT_FULL_LOAD;
            }
          }
        }
      }
    }
    in.close();
  }
  delete [] cstr;
  show(resultOfLoad_);
}

void Translate::addTranslation()
{
  QString str = lineEdit_->text();
  QString *array = new QString[3];
  array = toPrettyFormWordWithTrans(str, array);
  std::string word = array[0].toUtf8().constData();
  std::string lineOfTranslation = array[2].toUtf8().constData();
  int add = -2;
  if(word != WRONG_ENGLIH_WORD)
  {
    add =   add = hashTable_->addTranslation(word, lineOfTranslation);
  }
  else
  {
    add = -1;
  }
  QString result;
  if(add == 0)
  {
    result = TO_THIS_WORD;
    result += QString::fromStdString(word);
    result += ADD_TRANSLATIONS;
    result += QString::fromStdString(lineOfTranslation);
    show(result);
  }
  else if(add == -1)
  {
    result = WRONG_WORD;
    show(result);
  }
  else if(add == 1)
  {
    result = WORD_NOT_EXIST;
    show(result);
  }
  delete [] array;
}

void Translate::addWord()
{
  QString str = lineEdit_->text();
  QString result = insert(str);
  show(result);
}

void Translate::clearingTranslation()
{
  textEdit_->clear();
  lineEdit_->clear();
}

void Translate::translate()
{
  QString s = lineEdit_->text();
  std::string word = s.toUtf8().constData();
  word = toLower(word);
  QString result;
  std::string line;
  qDebug() << "translate mistake:" << QString::fromStdString(word);
  if(word != WRONG_ENGLIH_WORD && !word.empty())
  {
    line = hashTable_->translate(word);
    result = QString::fromStdString(line);
  }
  else
  {
    result = VALIDATION;
  }

  show(result);
}
