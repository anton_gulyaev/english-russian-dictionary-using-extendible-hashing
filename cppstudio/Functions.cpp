#include "Functions.hpp"

const std::string EMPTY_LINE = "-1";

std::string toLower(std::string word)
{
  std::string result;
  bool letter = false;
  if(word.empty())
  {
    result = EMPTY_LINE;
  }
  else
  {
    for (int i = 0; word[i] != '\0'; ++i)
    {
      if((word[i] >= 'A' && word[i] <= 'Z') ||  (word[i] >= 'a' && word[i] <= 'z'))
      {
        if (word[i] >= 'A' && word[i] <= 'Z')
        {
          word[i] = word[i] + 'a' - 'A';
        }
        letter = true;
      }
      else
      {
        if(word[i] == ' ')
        {
          word.erase(i, 1);
          --i;
        }
        else if(word[i] != '_' && word[i] != '-')
        {
          result = EMPTY_LINE;
          break;
        }
      }
    }
    if(result != EMPTY_LINE && letter == true)
    {
      result = word;
    }
  }
  qDebug() << "тупа строка:"<< QString::fromStdString(word);
  return result;
}
//поместить в toLower расшифровку всего, ищменить нащвание, сделать в претти норм редактирование всего
QString* toPrettyFormWordWithTrans(QString str, QString* array)
{
  QString englishWord = cutString(str);
  str.remove(0, englishWord.size());
  std::string word = toLower(englishWord.toUtf8().constData());
  englishWord = QString::fromStdString(word);
  array[0] = englishWord;
  qDebug() << "probeliiii" << " " << array[0];
  bool isTranscriptionOpen = false;
  bool iSTranscriptionClose = false;
  for(QChar i : str)
  {
    if(i == '[')
    {
      isTranscriptionOpen = true;
    }
    if(i == ']')
    {
      iSTranscriptionClose = true;
    }
  }
  QString transcription;
  if(isTranscriptionOpen == true && iSTranscriptionClose == true)
  {
    for(int i = 0; i < str.size(); ++i)
    {
      if(str[i] == '[')
      {
        while(str[i] != ']')
        {
          transcription += str[i];
          str.remove(i, 1);
        }
        transcription += str[i];
        str.remove(i, 1);
      }
    }
  }
  else
  {
    for(int i = 0; i < str.size(); ++i)   //удаление только открывающей или только закрывающей скобочки
    {
      if(str[i] == '[' || str[i] == ']')
      {
        str.remove(i, 1);
        --i;
      }
    }
    transcription = "[]";
  }
  transcription = removeSpacesInTranscription(transcription);
  array[1] = transcription;
  if(str != "")
    str = removeSpacesInTranslation(str);
  array[2] = str;
  return array;
}

QString cutString(QString str)
{
  QString englishWord;
  for (QChar i : str)
  {
    if (i == ' ')
    {
      break;
    }
    englishWord += i;
  }
  return englishWord;
}

QString removeSpacesInTranscription(QString line)
{
  for(int i = 0; i<line.size() - 1; ++i)
  {
    if(line[i] == ' ')
    {
      line.remove(i, 1);
      --i;
    }
  }
  return line;
}

QString removeSpacesInTranslation(QString line)
{
  for(int i = 0; i<line.size() - 1; ++i)
  {
    if(line[i] == ' ' && line[i + 1] == ' ')
    {
      line.remove(i, 1);
      --i;
    }
  }
  if(line[0] == ' ')
  {
    line.remove(0, 1);
  }
  return line;
}
