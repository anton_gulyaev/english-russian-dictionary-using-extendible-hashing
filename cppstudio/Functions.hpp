#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP
#include <iostream>
#include <QString>
#include <QDebug>
#include "lesson_2//DynamicArray.hpp"

std::string toLower(std::string word);
QString* toPrettyFormWordWithTrans(QString str, QString* array);
QString cutString(QString str);
QString removeSpacesInTranslation(QString line);
QString removeSpacesInTranscription(QString line);


#endif // FUNCTIONS_HPP
